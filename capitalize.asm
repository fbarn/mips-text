	.data

inputstring: 	.asciiz "Lorem ipsum dolor si amet"
newline:	.asciiz "\n"
outputstring:	.space 100


	.text
	.globl main
	
	main:
		li 	$t0, 0 			# Set counter to zero, and store it in register $t0
		li	$v0, 4			
		la	$a0, inputstring
		syscall
		li	$v0, 4
		la	$a0, newline
		syscall				# Print the (uncapitalized) input string, and start a new line
		
	iterate:				# Start a new set of steps "iterate"
		lb $t1, inputstring($t0)	# Load the $t0^th character of the input string
		beq $t1, 0, exit		# If this character represents the end of the string, proceed to "exit" 
		blt $t1, 'a', uncap		# Otherwise, check if it is a lowercase letter
		bgt $t1, 'z', uncap		# If it isn't, proceed to "uncap"
		sub $t1, $t1, 32		# If it is, capitalize it
		sb $t1, outputstring($t0)	# Store the newly-capitalized character into the $t0^th position of allocated space "outputstring"
		addi $t0, $t0, 1		# Increase counter $t0 by 1
		j iterate			# Repeat each "iterate" step until the end of the string is reached. 
		
	uncap:					# (Run if character in $t1 is not uncapitalized letter)
		sb $t1, outputstring($t0)	# Store the character as-is into the $t0^th position of allocated space "outputstring"
		addi $t0, $t0, 1		# Increase counter $t0 by 1
		j iterate			# Repeat each "iterate" step until the end of the string is reached. 
	exit:
		li	$v0, 4
		la	$a0, outputstring
		syscall
		li $v0, 10
		syscall				# Print the string stored in "outputstring"

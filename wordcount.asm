# This MIPS counts the occurence of a word in a text block using MMIO

.data

title:		.asciiz "Word count\n" #used to display name of program
prompt1:	.asciiz "Enter the text segment:\n\t" #used to prompt user for sentence
prompt2:	.asciiz "\nEnter the search word:\n\t" #used to prompt user for word
answer1:	.asciiz "\nThe word '"
answer2:	.asciiz "' occured "
answer3:	.asciiz " time(s).\n" #used to give answer for number of occurrences
finalprompt: 	.asciiz "press 'e' to enter another segment of text or 'q' to quit.\n\n" #asks user if they are done or if they want to go again
text:		.space 600
word: 		.space 600
times:		.space 4

	.text
	.globl main

main:	# all subroutines you create must come below "main"
	la $a0, title	#print title
	jal pprompt
fork:		# Essentially the main subroutine (Used so title is not printed over again)
	la $a0, prompt1
	jal pprompt		#ask user to enter sentence
	
	la $a0, text
	jal ginp		#store that sentence
	
	la $a0, prompt2
	jal pprompt		#ask user to enter word
	
	la $a0, word
	jal ginp		#store that word
	
	la $a0, text
	jal clean		#remove any character that is not alphanumeric or a space from the sentence
	
	la $a0,text
	la $a1, word
	jal find		#count the number of times the word occurs in this sentence
	
	add $a0, $0, $v0
	la $a1, times
	jal inttostring		# convert that number into a string, save it under times
	
	la $a0, answer1
	jal pprompt
	
	la $a0, word
	jal pprompt
	
	la $a0, answer2
	jal pprompt
	
	la $a0, times
	jal pprompt
	
	la $a0, answer3		# print the answer sentence
	jal pprompt
	
	la $a0, finalprompt	# ask user wether they want to quit or go again by entering 'e' or 'q'
	jal pprompt
	
	jal eorq		# save this value ('e' or 'q')
	
	beq $v0, 101, fork	# if user entered 'e', restart the main routine
	beq $v0, 113, quit	# otherwise, quit the program
	j eorq			# if user entered something else, ignore, and wait for them to enter an appropriate value
	
pprompt: #same as in quicksort
	lui $t0, 0xffff
	add $t3, $a0, $0	#load data to be printed
	
	phelper:
		lb $t2, ($t3) #Load each byte at a time
		lw $t1, 8($t0)
		andi, $t1, $t1, 0x0001 #wait for new byte to be loaded
		beqz, $t1, phelper # if it is not, keep waiting
		sw, $t2, 12($t0) #if it is , print it
		addi $t3, $t3, 1 # increase position of string by 1
		bne, $t2, 0, phelper # Do this until null character is reached in the string
		add $t0, $0, $0 # set all registers to 0
		add $t1, $0, $0
		add $t2, $0, $0
		add $t3, $0, $0
		jr $ra #jump to return address

ginp: # same as in quicksort
	lui $t0, 0xffff
	add $t3, $a0, $0 # load buffer to which data is to be written
	
	ihelper:
		lw $t1, 0($t0)
		andi, $t1, $t1, 0x0001
		beqz, $t1, ihelper #wait for a byte to be inputted. If it isn't, keep waiting
		lw, $t2, 4($t0)	
		sw, $t2, 12($t0) #if it is, print it...
		sb $t2, ($t3) # and save it to buffer
		addi $t3, $t3, 1 # increase buffer position by 1
		bne $t2, 10, ihelper # do this until user hits enter
		addi $t3, $t3, -1
		sb $0, ($t3) # make sure the buffer ends with null character
		add $t0, $0, $0 #reset all registers
		add $t1, $0, $0
		add $t2, $0, $0
		add $t3, $0, $0
		jr $ra # jump back to return address

clean:
	addi $t0, $a0, -1 

	cleanhelper:
		addi $t0, $t0, 1  #position was originally -1, so we are starting at 1
		lb $t2, ($t0) 		#load each byte of the input string
		beq $t2, 32, cleanhelper # if it is a space ignore it
		sge $t3, $t2, 48
		sle $t4, $t2, 57
		add $t3, $t3, $t4
		beq $t3, 2, cleanhelper # if it is a number ignore it
		sge $t3, $t2, 65
		sle $t4, $t2, 90
		add $t3, $t3, $t4
		beq $t3, 2, cleanhelper # if it is an upercase letter, ignore it
		sge $t3, $t2, 97
		sle $t4, $t2, 122
		add $t3, $t3, $t4
		beq $t3, 2, cleanhelper # if it is a lowercase letter ignore it
		add $t1, $0, 32		# if it is anything else, replace it with a space
		sb $t1, ($t0)		
		bgtz $t2, cleanhelper # if it was not a null character, repeat with the next character
		sb $t2, ($t0)		# otherwise, reset it to null and go back to return address
		jr $ra
		
find:
	add $t0, $a0, $0	#load address of word and sentence
	add $t1, $a1, $0
	add $t4, $0, $0
	
		fhelper:
			lb $t2, ($t0)	#load current byte of sentence
			lb $t3, ($t1)	#load current byte of word
			beq $t3, $0, addcount	#if end of word is reached, we add 1 to its occurrence
			beq $t2, $0, counted   #if end of sentence is reached, we are done
			beq $t2, 32, nextword	#if we hit a space in sentence, go to start of next word
			bne $t2, $t3, tospace	# if the sentence byte is an alphanum not equal to the word byte, go to next space
			addi $t1, $t1, 1	#if the sentence byte is an alphanum equal to the word byte, increase word position and sentence position by 1
			addi $t0, $t0, 1
			j fhelper		#repeat the routine with new values
							
			addcount:				#when we increase the occurrence
				beq $t2, $0, precounted		# if the next byte in the sentence is null, we finish the routine after increasing the count
				bne $t2, 32, tospace		# if it is neither a space or null, then we do not increase the count, and simply move to next space
				add $t1, $a0, $0		#otherwise, we reset position of the word, increase the count by 1, and repeat the subroutine
				addi $t4, $t4, 1
				j fhelper
				
			nextword:		#if we are at a space
				addi $t0, $t0, 1	#we load the next byte
				lb $t2, ($t0)		
				beq $t2, $0, counted	#if it is null, end
				beq $t2, 32, nextword	#if it is a space, repeat
				add $t1, $a1, $0	#if not, reset the word position, go back to looking for the word
				j fhelper

			tospace:		#if we are at a word which deosnt match
				addi $t0, $t0, 1
				lb $t2, ($t0)	#we load the next byte
				beq $t2, $0, counted	#if it is null, end
				bne $t2, 32, tospace	#if it is alphanum, repeat
				add $t1, $a1, $0	#if it is a space, go back to looking for the word
				j fhelper
				
			precounted:
				addi $t4, $t4, 1
			
			counted:
				add $v0, $0, $t4 	#save number of occurrences
				add $t0, $0, $0		#reset all values to 0
				add $t1, $0, $0
				add $t2, $0, $0
				add $t3, $0, $0
				add $t4, $0, $0
				jr $ra			# go to return address
			
eorq:
	lui $t0, 0xffff #same as ginp, except only one character is read
	
	ehelper:
		lw $t1, 0($t0)
		andi, $t1, $t1, 0x0001
		beqz, $t1, ehelper
		lb, $v0, 4($t0)	#save that character and return to address
		add $t0, $0, $0
		add $t1, $0, $0
		jr $ra

inttostring:
	add $t0, $0, $a0		#load the byte corresponding to the number of occurrences
	ble $t0, 9, one			#load the buffer array used to store it
	ble $t0, 99, two
	li $t1, 100			# if it is 3 digits long...
	div $t0, $t1
	mflo $t1
	mfhi $t2			
	li $t3, 10
	div $t2, $t3
	mflo, $t2
	mfhi, $t3
	addi $t1, $t1, 48
	addi $t2, $t2, 48
	addi $t3, $t3, 48
	sb $t1, ($a1)	# save the quotient of the occurrence and 100 in the first position
	sb $t2, 1($a1)	#save the quotient of the remainder of the previous operation and 10 in the second position
	sb $t3, 2($a1)	# save the remainder of the above operation in the third position
	sb $0, 3($a1)	#save the null byte in the fourth position
	jr $ra

	
	one:				#if it is 1 digit long
		addi $t0, $t0, 48
		sb $t0, ($a1)		#save it in the first position, and save null in sacond position
		sb $0, 1($a1)
		jr $ra
		
	two:				#if it is two digits long 
		li $t1 10
		div $t0, $t1
		mflo $t1
		mfhi $t2
		addi $t1, $t1, 48
		addi $t2, $t2, 48
		sb $t1, ($a1)		#save the quotient of the number and 10 in first position
		sb $t2, 1($a1)		#save the remainder in the second postion
		sb $0, 2($a1)		#save null in the third position
		jr $ra
		#in all cases, once this is done, return to address
	
quit: # quit program by using the exit syscall
	li $v0, 10
	syscall
